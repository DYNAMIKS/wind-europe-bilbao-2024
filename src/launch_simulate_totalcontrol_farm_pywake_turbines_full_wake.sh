#!/bin/bash
#
#SBATCH --job-name=simulate_totalcontrol_farm_pywake_turbines_full_wake
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --exclusive
#SBATCH --time=12:00:00
#SBATCH --partition=workq

module purge
module load Python/3.11.2-GCCcore-12.2.0-bare CMake/3.23.1-GCCcore-11.3.0
module load intel/19.0.4.243
source /apps/external/hpcx/2.5.0/MLNX_OFED_LINUX-4.6-1.0.1.1-redhat7.6-x86_64/ompi-mt-intel-19.0.4.243.sh
hpcx_load

rm simulate_totalcontrol_farm_pywake_turbines_full_wake.log

echo Run simulate_totalcontrol_farm_pywake_turbines_full_wake...
poetry run python simulate_totalcontrol_farm_pywake_turbines_full_wake.py

echo Done!
