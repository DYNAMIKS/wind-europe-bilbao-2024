# -*- coding: utf-8 -*-
"""
Run a simulation with the TOTALCONTROL farm and PyWake turbines in full wake using dynamiks.

@author: ricriv
"""

# %% Import.

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from cycler import cycler
from dynamiks.dwm import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator
from dynamiks.dwm.particle_motion_models import CutOffFrqLarsen2008, ParticleMotionModel
from dynamiks.dwm.particles_model import DistributedWindTurbinesParticles
from dynamiks.sites import TurbulenceFieldSite
from dynamiks.sites.mean_wind import ConstantWindSpeed
from dynamiks.sites.turbulence_fields import MannTurbulenceField
from dynamiks.utils.data_dumper import DataDumper
from dynamiks.views import XView, XYView, XZView
from dynamiks.wind_turbines import PyWakeWindTurbines
from py_wake.site.shear import PowerShear

from iea10_pywake import iea10mw
from totalcontrol_farm_layout import farm_layout

# Select Backend.
plt.close("all")
try:
    matplotlib.use("Qt5Agg")  # PC Windows.
except:
    matplotlib.use("Agg")  # Sophia.

matplotlib.rcdefaults()
plt.rc(
    "axes",
    prop_cycle=(
        cycler("linestyle", ["-", "--", ":", "-."])
        * cycler("color", plt.rcParams["axes.prop_cycle"].by_key()["color"])
    ),
)


# %%

# Required by H2Lib because of multiprocessing.
if __name__ == "__main__":
    # %% Define and make folders.

    sim_name = "totalcontrol_farm_pywake_turbines_full_wake"
    res_folder = f"../res/{sim_name}"
    fig_folder = f"../figures/{sim_name}"
    ani_folder = f"../animation/{sim_name}"

    os.makedirs(res_folder, exist_ok=True)
    os.makedirs(fig_folder, exist_ok=True)
    os.makedirs(ani_folder, exist_ok=True)

    # %% Generate turbulence box.

    # Turbulence box path.
    turbulence_box_folder = "../iea10mw_local/turb/"
    turbulence_box_name = "box_totalcontrol_farm_full_wake.nc"
    turbulence_box_path = f"{turbulence_box_folder}/{turbulence_box_name}"

    # Hub height of the IEA 10 MW [m].
    hub_height = 119.0

    # Rotor diameter of the IEA 10 MW [m].
    rotor_diameter = 198.0

    # Mean wind speed [m/s].
    ws = 9.0

    # Turbulence intensity [-].
    # ti = 0.16 * (0.75*ws + 5.6) / ws  # NTM.
    ti = 0.10

    # Simulation length [s].
    simulation_length = 600.0

    # Check if the turbulence box has already been generated.
    if not os.path.isfile(turbulence_box_path):
        # Number of points along u, v and w [-].
        nu = 2048
        nv = 2048
        nw = 64
        # For testing.
        # nu = 1024
        # nv = 1024
        # nw = 64

        # Turbulence box spacing along u, v and w [m].
        du = (ws * (simulation_length + 10) + 4000) / (nu - 1)
        dv = 8500.0 / (nv - 1)
        dw = 500.0 / (nw - 1)

        # Generate turbulence box.
        print("Generating turbulence box...")
        mtf = MannTurbulenceField.generate(
            alphaepsilon=0.1,
            L=33.6,
            Gamma=3.9,
            Nxyz=(nu, nv, nw),
            dxyz=(du, dv, dw),
            seed=123,
            HighFreqComp=1,
            double_xyz=(False, False, False),
        )

        # Export to netcdf.
        os.makedirs(turbulence_box_folder, exist_ok=True)
        mtf.to_netcdf(folder=turbulence_box_folder, filename=turbulence_box_name)

    else:
        print("Loading turbulence box...")
        mtf = MannTurbulenceField.from_netcdf(turbulence_box_path)

    print(
        f"Before: Box TI={mtf.uvw[0].std()/ws:.3f}, alphaepsilon:{mtf.alphaepsilon:.3f}, theoretical spectrum TI {mtf.spectrum_TI(ws):.2f}"
    )
    mtf.scale_TI(ti=ti, U=ws, T=simulation_length, cutoff_frq=10.0)
    print(
        f"After: Box TI={mtf.uvw[0].std()/ws:.3f}, alphaepsilon:{mtf.alphaepsilon:.3f}, theoretical spectrum TI {mtf.spectrum_TI(ws):.2f}"
    )

    if False:
        da = mtf.to_xarray()

        fig = plt.figure(constrained_layout=True)
        gs = fig.add_gridspec(
            nrows=3,
            ncols=2,
            figure=fig,
            width_ratios=[1, 0.05],
            height_ratios=[1, 1, 1],
        )
        ax_u_xy = fig.add_subplot(gs[0, 0])
        ax_u_xz = fig.add_subplot(gs[1, 0])
        ax_u_yz = fig.add_subplot(gs[2, 0])
        ax_cbar = fig.add_subplot(gs[:, 1])

        fig.suptitle("u [m/s]")

        ax_u_xy.set_xlabel("x [m] (along mean wind direction)")
        ax_u_xz.set_xlabel("x [m] (along mean wind direction)")
        ax_u_yz.set_xlabel("y [m]")

        ax_u_xy.set_ylabel("y [m]")
        ax_u_xz.set_ylabel("z [m] (vertical)")
        ax_u_yz.set_ylabel("z [m] (vertical)")

        norm = matplotlib.colors.Normalize(vmin=da.min().item(), vmax=da.max().item())

        X, Y = np.meshgrid(da.x.to_numpy(), da.y.to_numpy(), indexing="ij")
        contour = ax_u_xy.contourf(
            X, Y, da.sel(uvw="u", z=0.0).to_numpy(), levels=100, norm=norm
        )

        X, Z = np.meshgrid(da.x.to_numpy(), da.z.to_numpy(), indexing="ij")
        ax_u_xz.contourf(X, Z, da.sel(uvw="u", y=0.0).to_numpy(), levels=100, norm=norm)

        Y, Z = np.meshgrid(da.y.to_numpy(), da.z.to_numpy(), indexing="ij")
        ax_u_yz.contourf(Y, Z, da.sel(uvw="u", x=0.0).to_numpy(), levels=100, norm=norm)

        plt.colorbar(contour, cax=ax_cbar)

    # %% Define site.

    site = TurbulenceFieldSite(
        ws=ConstantWindSpeed(
            ws,
            shear=PowerShear(h_ref=hub_height, alpha=0.2),
        ),
        turbulenceField=mtf,
        turbulence_offset=np.array(
            [
                -ws * (simulation_length + 10) + 5000,
                4000.0,
                0.0,
            ]
        ),
    )

    if False:
        time = range(int(simulation_length))
        view = XView(x=range(int(simulation_length)), y=0.0, z=hub_height)
        uvw = site.get_windspeed(view)
        fig, ax = plt.subplots(dpi=300)
        ax.set_xlabel("Time [s]")
        ax.set_ylabel("Wind speed [m/s]")
        ax.grid(True)
        for i, n in enumerate("uvw"):
            ax.plot(uvw[i], label=n)
        ax.legend()
        plt.tight_layout()

    # %% Define turbines.

    wts = PyWakeWindTurbines(
        x=farm_layout[:, 0], y=farm_layout[:, 1], windTurbine=iea10mw
    )

    # Plot turbine positions.
    fig, ax = plt.subplots()
    ax.set_xlabel("West - East [m]")
    ax.set_ylabel("South - North [m]")
    ax.grid(True)
    ax.scatter(farm_layout[:, 0], farm_layout[:, 1])
    for i in range(farm_layout.shape[0]):
        ax.text(farm_layout[i, 0], farm_layout[i, 1], f"{i}")
    ax.axis("equal")

    # %% Wind farm simulation.

    # Record data.
    dump_rotor_avg_ws = DataDumper(
        lambda sim: sim.windTurbines.rotor_avg_windspeed(include_wakes=True).T,
        coords={"wt": wts.idx, "uvw": ["u", "v", "w"]},
    )
    dump_power = DataDumper(
        lambda sim: sim.windTurbines.power(include_wakes=True),
        coords={"wt": wts.idx},
    )

    # Setup simulation.
    fs = DWMFlowSimulation(
        site=site,
        windTurbines=wts,
        particleDeficitGenerator=jDWMAinslieGenerator(),
        dt=1.0,
        particleMotionModel=ParticleMotionModel(CutOffFrqLarsen2008),
        d_particle=0.2,  # distance between particles, normalized with wind turbine diameter
        step_handlers=[dump_rotor_avg_ws, dump_power],
        windTurbinesParticles=DistributedWindTurbinesParticles,
    )

    # Visualize flow at the beginning of the simulation.
    fig, ax = plt.subplots()
    fs.visualize(
        fs.time + 2,
        view=XYView(
            z=hub_height,
            xlim=(5000, 10000),
            ylim=(4000, 12000),
            ax=ax,
        ),
    )
    # fig.savefig(f"{fig_folder}/flow_xy_view_beginning_simulation.pdf")
    fig.savefig(f"{fig_folder}/flow_xy_view_beginning_simulation.png", dpi=300)

    fig, ax = plt.subplots()
    fs.visualize(
        fs.time + 2,
        view=XZView(
            y=0.0,
            xlim=(5000, 10000),
            ax=ax,
        ),
    )
    # fig.savefig(f"{fig_folder}/flow_xz_view_beginning_simulation.pdf")
    fig.savefig(f"{fig_folder}/flow_xz_view_beginning_simulation.png", dpi=300)

    # Sun simulation.
    fs.run(296)

    # Visualize flow at the end of the simulation.
    fig, ax = plt.subplots()
    fs.visualize(
        fs.time + 2,
        view=XYView(
            z=hub_height,
            xlim=(5000, 10000),
            ylim=(4000, 12000),
            ax=ax,
        ),
    )
    # fig.savefig(f"{fig_folder}/flow_xy_view_end_simulation.pdf")
    fig.savefig(f"{fig_folder}/flow_xy_view_end_simulation.png", dpi=300)

    fig, ax = plt.subplots()
    fs.visualize(
        fs.time + 2,
        view=XZView(
            y=0.0,
            xlim=(5000, 10000),
            ax=ax,
        ),
    )
    # fig.savefig(f"{fig_folder}/flow_xz_view_end_simulation.pdf")
    fig.savefig(f"{fig_folder}/flow_xz_view_end_simulation.png", dpi=300)

    # Save results.
    xr_rotor_avg_ws = dump_rotor_avg_ws.to_xarray()
    xr_power = dump_power.to_xarray()
    xr_rotor_avg_ws.to_netcdf(f"{res_folder}/rotor_avg_ws.nc")
    xr_power.to_netcdf(f"{res_folder}/power.nc")

    # Plot results.

    # Plot rotor average wind speed.
    fig, ax = plt.subplots()
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Rotor average wind speed, u [m/s]")
    ax.grid(True)
    for wt in wts.idx:
        ax.plot(xr_rotor_avg_ws.sel(wt=wt)[:, 0], label=f"WT {wt}")
    ax.legend(bbox_to_anchor=(1.04, 1), loc="upper left")
    plt.subplots_adjust(right=0.8)
    fig.savefig(f"{fig_folder}/rotor_avg_wsp_u.pdf")
    fig.savefig(f"{fig_folder}/rotor_avg_wsp_u.png", dpi=300)

    # Plot power.
    fig, ax = plt.subplots()
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Power [kW]")
    ax.grid(True)
    for wt in wts.idx:
        ax.plot(xr_power.sel(wt=wt) / 1000, label=f"WT {wt}")
    ax.legend(bbox_to_anchor=(1.04, 1), loc="upper left")
    plt.subplots_adjust(right=0.8)
    fig.savefig(f"{fig_folder}/aerodynamic_power.pdf")
    fig.savefig(f"{fig_folder}/aerodynamic_power.png", dpi=300)
