# -*- coding: utf-8 -*-
"""
PyWake model of the IEA 10 MW wind turbine.
The data are computed with HAWCStab2 2.16.

@author: ricriv
"""

# %% Import.

import numpy as np
from py_wake.wind_turbines import WindTurbine
from py_wake.wind_turbines.power_ct_functions import PowerCtTabular

# %% Make PyWake object.

_ws = np.array(
    [
        0.400e01,
        0.400e01 + 1e-8,  # Added to avoid vertical slope from cut-in.
        0.500e01,
        0.600e01,
        0.700e01,
        0.800e01,
        0.900e01,
        0.950e01,
        0.100e02,
        0.105e02,
        0.110e02,
        0.115e02,
        0.120e02,
        0.130e02,
        0.140e02,
        0.150e02,
        0.160e02,
        0.180e02,
        0.250e02,
        0.200e02,
    ]
)

_power = (
    np.array(
        [
            0.4576793179e03,
            0.4576793179e03,  # Copied from previous wind speed.
            0.1096895575e04,
            0.1998941533e04,
            0.3173741269e04,
            0.4721619344e04,
            0.6696464501e04,
            0.7812588874e04,
            0.8992247248e04,
            0.1023362045e05,
            0.1063825624e05,
            0.1063829457e05,
            0.1063832297e05,
            0.1063829550e05,
            0.1063833555e05,
            0.1063828743e05,
            0.1063825260e05,
            0.1063831270e05,
            0.1063833853e05,
            0.1063824313e05,
        ]
    )
    * 0.1e5
    / 0.1063833853e05  # Generator efficiency = 0.94
)

_ct = np.array(
    [
        0.9291046450e00,
        0.9291046450e00,  # Copied from previous wind speed.
        0.9222191787e00,
        0.8955751574e00,
        0.8847544112e00,
        0.8729437708e00,
        0.8291574448e00,
        0.7909399328e00,
        0.7555490582e00,
        0.7226807542e00,
        0.5932258079e00,
        0.4892057184e00,
        0.4163504058e00,
        0.3159005198e00,
        0.2488334513e00,
        0.2010490027e00,
        0.1656356504e00,
        0.1176370815e00,
        0.4838221935e-01,
        0.8756318433e-01,
    ]
)


iea10mw = WindTurbine(
    name="IEA10MW",
    diameter=198.0,
    hub_height=119.0,
    powerCtFunction=PowerCtTabular(
        ws=_ws,
        power=_power,
        power_unit="kW",
        ct=_ct,
        ws_cutin=4.0,
        ws_cutout=25.0,
        power_idle=0.0,
        ct_idle=0.3e-02,
        method="pchip",
    ),
)

if __name__ == "__main__":
    # Plot power and Ct curves.
    import matplotlib.pyplot as plt

    ws = np.arange(0.0, 27.1, 0.1)

    fig, ax = plt.subplots()
    ax.set_xlabel("Wind speed [m/s]")
    ax.set_ylabel("Power [kW]")
    ax.grid(True)
    ax.plot(ws, iea10mw.power(ws) / 1000)

    fig, ax = plt.subplots()
    ax.set_xlabel("Wind speed [m/s]")
    ax.set_ylabel("Thrust coefficient [-]")
    ax.grid(True)
    ax.plot(ws, iea10mw.ct(ws))
