#!/bin/bash
#
#SBATCH --job-name=fast_dynamiks_simulations
#SBATCH --output=fast_dynamiks_simulations.log
#SBATCH --error=fast_dynamiks_simulations.log
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --exclusive
#SBATCH --time=01:00:00
#SBATCH --partition=workq

module purge
module load Python/3.11.2-GCCcore-12.2.0-bare CMake/3.23.1-GCCcore-11.3.0
module load intel/19.0.4.243
source /apps/external/hpcx/2.5.0/MLNX_OFED_LINUX-4.6-1.0.1.1-redhat7.6-x86_64/ompi-mt-intel-19.0.4.243.sh
hpcx_load

rm fast_dynamiks_simulations.log

rm simulate_2_hawc2_turbines_full_wake.log   simulate_2_hawc2_turbines_partial_wake.log
rm simulate_2_pywake_turbines_full_wake.log  simulate_2_pywake_turbines_partial_wake.log

echo Run simulate_2_hawc2_turbines_full_wake...
poetry run python simulate_2_hawc2_turbines_full_wake.py       &>>  simulate_2_hawc2_turbines_full_wake.log      &

echo Run simulate_2_hawc2_turbines_partial_wake...
poetry run python simulate_2_hawc2_turbines_partial_wake.py    &>>  simulate_2_hawc2_turbines_partial_wake.log   &

echo Run simulate_2_pywake_turbines_full_wake...
poetry run python simulate_2_pywake_turbines_full_wake.py      &>>  simulate_2_pywake_turbines_full_wake.log     &

echo Run simulate_2_pywake_turbines_partial_wake...
poetry run python simulate_2_pywake_turbines_partial_wake.py   &>>  simulate_2_pywake_turbines_partial_wake.log  &

wait

echo Done!
