# -*- coding: utf-8 -*-
"""
Simulate 1 HAWC2 simulation with dynamiks.

@author: ricriv
"""

# %% Import.

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from h2lib import H2Lib
from hipersim import MannTurbulenceField

# Select Backend.
plt.close("all")
try:
    matplotlib.use("Qt5Agg")  # PC Windows.
except:
    matplotlib.use("Agg")  # Sophia.


# Required by H2Lib because of multiprocessing.
if __name__ == "__main__":
    # %% Generate turbulence box.

    if True:
        # Hub height of the IEA 10 MW [m].
        hub_height = 119.0

        # Rotor diameter of the IEA 10 MW [m].
        rotor_diameter = 198.0

        # Mean wind speed [m/s].
        ws = 12.0

        # Turbulence intensity [-].
        ti = 0.05

        # Simulation length [s].
        simulation_length = 600.0

        # Number of points along u, v and w [-].
        nu = 8192
        nv = nw = 32

        # Turbulence box spacing along u, v and w [m].
        du = ws * simulation_length / nu
        dv = rotor_diameter * 1.15 / (nv - 1)
        dw = rotor_diameter * 1.15 / (nw - 1)

        # Generate turbulence box.
        mtf = MannTurbulenceField.generate(
            alphaepsilon=0.1,
            L=33.6,
            Gamma=3.9,
            Nxyz=(nu, nv, nw),
            dxyz=(du, dv, dw),
            seed=123,
            HighFreqComp=1,
            double_xyz=(False, False, False),
        )

        print(
            f"Before: Box TI={mtf.uvw[0].std()/ws:.3f}, alphaepsilon:{mtf.alphaepsilon:.3f}, theoretical spectrum TI {mtf.spectrum_TI(ws):.2f}"
        )
        mtf.scale_TI(ti=ti, U=ws, T=simulation_length, cutoff_frq=10.0)
        print(
            f"After: Box TI={mtf.uvw[0].std()/ws:.3f}, alphaepsilon:{mtf.alphaepsilon:.3f}, theoretical spectrum TI {mtf.spectrum_TI(ws):.2f}"
        )

        # Export to HAWC2 format.
        os.makedirs("../iea10mw_local/turb", exist_ok=True)
        mtf.to_hawc2(folder="../iea10mw_local/turb", basename="wind_1_box_")

        # Plot turbulence box in XZ plane for y = 0.
        da = mtf.to_xarray()

        if False:
            fig = plt.figure(dpi=300, constrained_layout=True)
            gs = fig.add_gridspec(
                nrows=3,
                ncols=2,
                figure=fig,
                width_ratios=[1, 0.05],
                height_ratios=[1, 1, 1],
            )
            ax_w = fig.add_subplot(gs[2, 0])
            ax_u = fig.add_subplot(gs[0, 0], sharex=ax_w)
            ax_v = fig.add_subplot(gs[1, 0], sharex=ax_w)
            ax_cbar = fig.add_subplot(gs[:, 1])
            ax_u.tick_params("x", labelbottom=False)
            ax_v.tick_params("x", labelbottom=False)

            ax_u.set_ylabel("u [m/s]")
            ax_v.set_ylabel("v [m/s]")
            ax_w.set_ylabel("w [m/s]")
            ax_w.set_xlabel("x [m]")

            norm = matplotlib.colors.Normalize(
                vmin=da.min().item(), vmax=da.max().item()
            )

            X, Z = np.meshgrid(da.x.to_numpy(), da.z.to_numpy(), indexing="ij")

            contour_u = ax_u.contourf(
                X, Z, da.sel(uvw="u", y=0.0).to_numpy(), levels=100, norm=norm
            )
            contour_v = ax_v.contourf(
                X, Z, da.sel(uvw="v", y=0.0).to_numpy(), levels=100, norm=norm
            )
            contour_w = ax_w.contourf(
                X, Z, da.sel(uvw="w", y=0.0).to_numpy(), levels=100, norm=norm
            )
            plt.colorbar(contour_u, cax=ax_cbar)

    # %% Simulate 1 turbine.

    with H2Lib() as h2:
        h2.init(htc_path="htc/one_turbine_dlc_1_2.htc", model_path="../iea10mw_local/")
        h2.run(10.0)
