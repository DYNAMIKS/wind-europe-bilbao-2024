# DYNAMIKS simulations for Wind Europe 2024 in Bilbao

[![pipeline status](https://gitlab.windenergy.dtu.dk/ricriv/wind-europe-bilbao-2024/badges/main/pipeline.svg)](https://gitlab.windenergy.dtu.dk/ricriv/wind-europe-bilbao-2024/-/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Poetry](https://img.shields.io/endpoint?url=https://python-poetry.org/badge/v0.json)](https://python-poetry.org/)

Contains `DYNAMIKS` simulations for Wind Europe 2024 in Bilbao. Half of the simulation use the simple and fast `PyWake` turbines, and the other half the more complex and slower `HAWC2` turbines.

The simulations are based on the IEA 10 MW, which is imported with a submodule inside `iea10mw_local`. The controller libraries are contained in `iea10mw_local/control`, which contains the Win32, Win64 and Linux versions. In all cases, The turbulence box is generated using `hipersim`. The `PyWake` model of the IEA 10 MW has been obtained with `HAWCStab2`.


The folder `src` contains the examples.

- `simulate_1_hawc2_turbine.py` simulates 1 turbine using `HAWC2lib`. The associated `HAWC2` model is `one_turbine_dlc_1_2.htc`.
- `simulate_2_pywake_turbines_full_wake.py` simulates 2 `PyWake` turbines in full wake using `DYNAMIKS`.
- `simulate_2_pywake_turbines_partial_wake.py` simulates 2 `PyWake` turbines in partial wake using `DYNAMIKS`.
- `simulate_2_hawc2_turbines_full_wake.py` simulates 2 `HAWC2` turbines in full wake using `DYNAMIKS`. The associated `HAWC2` model is `turbine4farm_no_output.htc`.
- `simulate_2_hawc2_turbines_partial_wake.py` simulates 2 `HAWC2` turbines in partial wake using `DYNAMIKS`. The associated `HAWC2` model is `turbine4farm_no_output.htc`.
- `simulate_totalcontrol_farm_pywake_turbines_full_wake.py` simulates the TOTALCONTROL wind farm using `PyWake` turbines in full wake.
- `simulate_totalcontrol_farm_pywake_turbines_partial_wake.py` simulates the TOTALCONTROL wind farm using `PyWake` turbines in partial wake.
- `simulate_totalcontrol_farm_hawc2_turbines_full_wake.py` simulates the TOTALCONTROL wind farm using `HAWC2` turbines in full wake.
- `simulate_totalcontrol_farm_hawc2_turbines_partial_wake.py` simulates the TOTALCONTROL wind farm using `HAWC2` turbines in partial wake.


## How to clone this repo on the DTU Sophia cluster

Start by requesting a node
```bash
srun --partition workq --nodes 1 --ntasks 1 --cpus-per-task=32 --exclusive --time 24:00:00 --pty bash
```
This will allow using the MultiProcess runner of HAWC2Lib. Please check [the guide](https://hawc2.pages.windenergy.dtu.dk/HAWC2Lib/ParallelPerformance.html) for how to use the MPI runner.

Then load git and clone this repo
```bash
module load git/2.36.0-GCCcore-11.3.0-nodocs
module load git-lfs/2.13.2
git clone --recurse-submodules https://gitlab.windenergy.dtu.dk/ricriv/wind-europe-bilbao-2024.git
```

It will ask for your username and a personal access token.

# How to install the dependencies.

The dependencies are managed via [Poetry](https://python-poetry.org/). You should thus start by installing Poetry
```bash
module load Python/3.11.2-GCCcore-12.2.0-bare
pip install --user poetry
```

And then you can install the dependencies
```bash
cd wind-europe-bilbao-2024
poetry install
```
This repo includes the lock file, and since DYNAMIKS is still developing quickly, it is recommanded to *not* update it. If you want to run the examples from Spyder, then you should write
```bash
poetry install --extras "spyder"
```


# HAWC2 license
To run the HAWC2 wind turbine examples, a valid HAWC2 license is required.

**New online license manager**

If you are on the DTU network:
```bash
mkdir ~/.config/hawc2 -p && echo "[licensing]
host = http://license-internal.windenergy.dtu.dk
port = 34523" > ~/.config/hawc2/license.cfg
```

Oterwise follow the instruction that came with the HAWC2 license.

**Old license manager**
Alternatively, you can use the old license manager

Copy the HAWC2 license manager (HAWC2License.so) to the h2lib package folder in the virtual python
```bash
~/.cache/pypoetry/virtualenvs/<ENVIRONMENT_NAME>/lib/python3.11/site-packages/h2lib
```
Poetry will write its name, or otherwise check with 
```bash
ls ~/.cache/pypoetry/virtualenvs/
```


# How to run simulations

Copy this command to the .bashrc file
```bash
alias py311intel='module purge && 
ml Python/3.11.2-GCCcore-12.2.0-bare CMake/3.23.1-GCCcore-11.3.0 && 
module load intel/19.0.4.243 && 
source /apps/external/hpcx/2.5.0/MLNX_OFED_LINUX-4.6-1.0.1.1-redhat7.6-x86_64/ompi-mt-intel-19.0.4.243.sh && 
hpcx_load'
```
and run it.

Finally, run any script with
```bash
cd src
poetry run python my_script.py
```

After the first installation, we can skip `poetry install` and directly type
```bash
py311intel
cd wind-europe-bilbao-2024/src
poetry run python my_script.py
```

You can launch the simulations on Sophia with
```bash
sbatch launch_fast_simulations.sh
sbatch launch_simulate_totalcontrol_farm_pywake_turbines_full_wake.sh
sbatch launch_simulate_totalcontrol_farm_pywake_turbines_partial_wake.sh
sbatch launch_simulate_totalcontrol_farm_hawc2_turbines_full_wake.sh
sbatch launch_simulate_totalcontrol_farm_hawc2_turbines_partial_wake.sh
```

and monitor their execution with
```bash
squeue --format="%.18i %.9P %.60j %.8u %.8T %.10M %.9l %.6D %R" -u $USER
```
